# CS 145 ME5 (Text File Transfer) #

## Usage ##

### Set up a server ###

```
$ java server.Main
```

1. You may enter the port number as a command-line argument. **Default:** 8888.

### Open a client ###

```
$ java client.Main
$ Enter a message: GET test.txt
```

1. You may enter the server's IP address and port number as a command-line argument. **Format:** 11.22.33.44:5555. **Default:** 127.0.0.1:8888.
2. Input "GET <filename>" to request a file from the server to copy to the local directory.
3. The server looks in the "server" directory and the client saves to the "client" directory.

## Build Details ##

Developed on NetBeans IDE 8.0.2 using Oracle Java 1.8.0_40.