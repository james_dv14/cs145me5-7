package server;

/**
 *
 * @author MARIANO, J Stephen DV
 * @studentno 2012-78002
 */
import java.io.*;
import java.net.*;
import java.util.Scanner;

public class MyConnection {
  //***************************************
  // Fields
  //***************************************
  private final Scanner in;
  private final PrintWriter out;
  //***************************************
  // Constructor
  //***************************************
  public MyConnection(Socket s) throws IOException {
    in = getInputScanner(s);
    out = getOutputWriter(s);
  }
  //***************************************
  // Accessors (message)
  //***************************************
  public boolean sendMessage(String msg) {
    try {
      out.println(msg);
      out.flush();
      return true;
    }	
    catch (Exception e) {
      return false;
    }	
  }

  public String receiveMessage() {
    try {
      return in.nextLine().trim();
    }
    catch (Exception e) {
      return MyServer.ERROR;
    }
  }
  //***************************************
  // I/O stream initializers
  //***************************************
  private Scanner getInputScanner(Socket s) throws IOException {
    InputStream is = s.getInputStream();
    InputStreamReader isr = new InputStreamReader(is);
    return new Scanner(new BufferedReader(isr));
  }

  private PrintWriter getOutputWriter(Socket s) throws IOException {
    OutputStream os = s.getOutputStream();
    OutputStreamWriter osw = new OutputStreamWriter(os);
    return new PrintWriter(osw);
  }
}
