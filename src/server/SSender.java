package server;

import java.io.FileNotFoundException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 *
 * @author MARIANO, J Stephen DV
 * @studentno 2012-78002
 */
public class SSender {
  //***************************************
  // Objects
  //***************************************
  private static SReader sr;
  //***************************************
  // Main method
  //***************************************
  public static void sendResponse(MyConnection c, String path, String protocol) {
    sendResponseHeader(c, protocol);
    sendResponseContent(c, path);
  }
  //***************************************
  // Send response header
  //***************************************
  private static void sendResponseHeader(MyConnection c, String protocol) {
    c.sendMessage(protocol + " 200 OK");
    c.sendMessage("Connection close");
    c.sendMessage("Date: " + getTimestamp());
    c.sendMessage("Server: " + SSender.class);
    c.sendMessage("Last-Modified: " + getTimestamp());
    c.sendMessage("Content-Length: 121");
    c.sendMessage("Content-Type: text/html");
    c.sendMessage("");
  }
  //***************************************
  // Send response content
  //***************************************
  private static void sendResponseContent(MyConnection c, String path) {
    AtomicBoolean good = new AtomicBoolean(true);
    try {
      sr = new SReader("server/" + path);
      while (good.get())
        c.sendMessage(sr.getLine(good));
    } 
    catch (FileNotFoundException ex) {
      c.sendMessage(MyServer.ERROR);
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }
  //***************************************
  // Auxiliary methods
  //***************************************
  /**
   * @credit: http://stackoverflow.com/questions/308683/how-can-i-get-the-current-date-and-time-in-utc-or-gmt-in-java
   */
  private static String getTimestamp() {
    SimpleDateFormat f = new SimpleDateFormat("yyyy-MMM-dd HH:mm:ss");
    f.setTimeZone(TimeZone.getTimeZone("UTC"));
    return f.format(new Date());
  }
}
