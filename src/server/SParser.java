package server;

/**
 *
 * @author MARIANO, J Stephen DV
 * @studentno 2012-78002
 */
public class SParser {
  
  public static void parseRequest(MyConnection c, String request) {
    String[] request_arr = request.split(" ", 3);
    try {
      if (request_arr[0].equals("GET"))
        SSender.sendResponse(c, request_arr[1], request_arr[2]);
    }
    catch (ArrayIndexOutOfBoundsException e) {
      System.err.println("Invalid HTTP request syntax.");
    }
  }
  
}
