package server;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 *
 * @author MARIANO, J Stephen DV
 * @studentno 2012-78002
 */
public class SReader {
  //***************************************
  // Objects
  //***************************************
  private final File file;
  private final Scanner scanner;
  //***************************************
  // Constructor
  //***************************************
  public SReader (String filepath) throws FileNotFoundException {
    file = new File(filepath);
    scanner = new Scanner(file);
  }
  //***************************************
  // Main method
  //***************************************
  public String getLine(AtomicBoolean good) {
    if (scanner.hasNextLine())
      return scanner.nextLine();

    System.err.println("NOTE: End of file reached.");
    scanner.close();
    good.set(false);
    return "";
  }
}
