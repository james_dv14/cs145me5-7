package server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 *
 * @author MARIANO, J Stephen DV
 * @studentno 2012-78002
 */
public class Helper {
  //***************************************
  // Prompts
  //***************************************

  /**
   * Returns console input the user provides in response to a given prompt.
   * @return The console input as a string.
   */
  public static String promptMessage() {
    String msg;
    System.out.print("Enter Message: ");
    BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
    try {
      msg = input.readLine();
    }
    catch (IOException e) {
      msg = "";
    }
    return msg;
  }

  //***************************************
  // Validation
  //***************************************

  /**
   * Checks if a given string is a valid IPv4 address
   * @credit StackOverflow user rouble
   * @source "Validating IPv4 string in Java". StackOverflow.
   * @url http://stackoverflow.com/questions/4581877/validating-ipv4-string-in-java
   * @param ip The candidate IP address, as a string.
   * @return True if the string is a valid IPv4 address, false otherwise.
   */
  public static boolean validIPv4 (String ip) {
    try {
      // Check if string is null or empty.
      if (ip == null || ip.isEmpty())
        return false;
      // Check if string has 4 parts.
      String[] parts = ip.split("\\.", -1);
      if ( parts.length != 4 )
        return false;
      // Check if each part is an integer from 0-255.
      for ( String s : parts ) {
        int i = Integer.parseInt( s );
        if (i < 0 || i > 255)
          return false;
      }
      //
      return true;
    } 
    catch (NumberFormatException nfe) {
      return false;
    }
  }

  /**
   * Checks if a given string is a valid port number (0-65535).
   * @param port The candidate port number, as a string.
   * @return  True if the string is a valid port number, false otherwise.
   */
  public static boolean validPort (String port) {
    try {
      // Check if string is null or empty.
      if (port == null || port.isEmpty())
        return false;
      // Check if the string is an integer from 0-65535.
      int i = Integer.parseInt(port);
      return !(i < 0 || i > 65535);
    } 
    catch (NumberFormatException nfe) {
      return false;
    }
  }

}
