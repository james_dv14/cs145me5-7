package server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 *
 * @author MARIANO, J Stephen DV
 * @studentno 2012-78002
 */
public class MyServer {
  //***************************************
  // Constants
  //***************************************
  public static final String ERROR = "Something bad happened :(";
  //***************************************
  // Objects
  //***************************************
  private static int port;
  private static MyConnection c;
  private static Socket s;
  private static ServerSocket ss;
  //***************************************
  // Main method
  //***************************************
  public static void main(String args[]) {
    setPort(args);
    try {
      ss = new ServerSocket(port);
      System.out.println("S: Starting server...");
      c = waitForConnection(ss);
      while (true)
        SParser.parseRequest(c, c.receiveMessage());
    } 
    catch (Exception e) {
      System.out.println("S: " + MyServer.ERROR);
      e.printStackTrace();
      System.exit(0);
    }
  }
  //***************************************
  // Auxiliary methods
  //***************************************
  private static void setPort(String[] args) {
    try {
      port = Helper.validPort(args[0]) ? Integer.parseInt(args[0]) : 8888;
    }
    catch (Exception e) {
      port = 8888;
    }
  }

  private static MyConnection waitForConnection(ServerSocket ss) throws IOException {
    System.out.println("S: Waiting for connections...");
    s = ss.accept();
    c = new MyConnection(s);
    System.out.println("S: Somebody connected!");
    return c;
  }
}
